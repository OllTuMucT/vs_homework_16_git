#include <iostream>
using namespace std;
int main()
{
	const int n = 10;
	int Array[n][n];
	for (int i = 0; i < n; i++)
	{
		for (int j = 0; j < n; j++)
		{
			Array[i][j] = i + j;
			cout << Array[i][j] << "\t";
		}
		cout << endl;
	}

	int Day;
	cout << "\n" << "Day: ";
	cin >> Day;
	cout << endl;

	const int row_index = Day % n;
	int sum = 0;

	for (int j = 0; j < n; j++)
	{
		sum += Array[row_index][j];
	}
	cout << "Line number: " << row_index << endl << "Amount: " << sum << endl;
}